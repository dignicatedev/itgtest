package com.dignicate.itgtest.page.impl;

import com.dignicate.itgtest.page.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 */
public class NextPage extends PageObject {

    public NextPage(WebDriver driver) {
        super(driver);
    }

    public LastPage clickNext() {
        WebElement next = driver.findElement(By.id("next"));
        Actions actions = new Actions(driver);
        actions.moveToElement(next);
        actions.perform();

        sleep(1000);
        driver.findElement(By.id("info1")).sendKeys("First Information");
        sleep(1000);
        driver.findElement(By.id("info2")).sendKeys("Second Information");
        sleep(1000);
        driver.findElement(By.id("next")).click();
        return new LastPage(driver);
    }

}
