package com.dignicate.itgtest.page.impl;

import com.dignicate.itgtest.page.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class SignInPage extends PageObject {

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public NextPage signIn(String user, String pass) {
        sleep(1000);
        driver.findElement(By.id("user")).sendKeys(user);
        sleep(1000);
        driver.findElement(By.id("pass")).sendKeys(pass);
        sleep(1000);
        driver.findElement(By.id("signIn")).click();
        return new NextPage(driver);
    }

}
