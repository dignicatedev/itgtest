package com.dignicate.itgtest.page.impl;

import com.dignicate.itgtest.page.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class LastPage extends PageObject {

    public LastPage(WebDriver driver) {
        super(driver);
    }

    public SignInPage clickTop() {
        sleep(2000);
        driver.findElement(By.id("top")).click();
        return new SignInPage(driver);
    }

}
