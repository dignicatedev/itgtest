package com.dignicate.itgtest.test.impl;

import com.dignicate.itgtest.page.impl.LastPage;
import com.dignicate.itgtest.page.impl.NextPage;
import com.dignicate.itgtest.page.impl.SignInPage;
import com.dignicate.itgtest.test.SeleniumTest;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class DummySiteTest extends SeleniumTest {

    @Test
    public void testSample() {
        WebDriver driver = open();
        SignInPage signInPage = new SignInPage(driver);
        NextPage nextPage = signInPage.signIn("user_name", "password");
        LastPage lastPage = nextPage.clickNext();
        lastPage.clickTop();
    }

    @After
    public void tearDown() {
        close(3000);
    }
}