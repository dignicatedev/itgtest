package com.dignicate.itgtest.test;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.URL;

/**
 *
 */
public abstract class SeleniumTest {

    private static final String TOP_URL = "http://dignicate.com/?p=1672#dummy_site";

    private final WebDriver driver;


    protected WebDriver open() {
        driver.get(TOP_URL);
        return driver;
    }

    protected SeleniumTest() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
            driver.manage().window().setSize(new Dimension(1400, 780));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected void close(long milliSec) {
        if (driver != null) {
            try {
                Thread.sleep(milliSec);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            driver.close();
        }
    }
}
